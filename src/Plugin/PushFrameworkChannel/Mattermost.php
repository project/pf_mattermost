<?php

namespace Drupal\pf_mattermost\Plugin\PushFrameworkChannel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\push_framework\ChannelBase;
use Drupal\user\UserInterface;
use Gnello\Mattermost\Driver;
use Markdownify\Converter;
use Pimple\Container;

/**
 * Plugin implementation of the push framework channel.
 *
 * @ChannelPlugin(
 *   id = "mattermost",
 *   label = @Translation("Mattermost"),
 *   description = @Translation("Provides the Mattermost channel plugin.")
 * )
 */
class Mattermost extends ChannelBase {

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'pf_mattermost.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(UserInterface $user): bool {
    // This channel sends directly, so it never applies to other notifications.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function send(UserInterface $user, ContentEntityInterface $entity, array $content, int $attempt): string {
    $container = new Container([
      'driver' => [
        'url' => $this->pluginConfig->get('domain'),
        'token' => $this->pluginConfig->get('token'),
      ],
      'guzzle' => [],
    ]);
    $driver = new Driver($container);
    $result = $driver->authenticate();
    if ($result->getStatusCode() === 200) {
      $converter = new Converter(Converter::LINK_AFTER_CONTENT, intval(FALSE), FALSE);
      $output = array_shift($content);
      $message = '<h1>' . $output['subject'] . '</h1>' . PHP_EOL . '<div>' . $output['body'] . '</div>';
      $result = $driver->getPostModel()->createPost([
        'channel_id' => $this->pluginConfig->get('channel_id'),
        'message' => strip_tags($converter->parseString($message)),
      ]);
      if ($result->getStatusCode() === 201) {
        return self::RESULT_STATUS_SUCCESS;
      }
    }
    return self::RESULT_STATUS_FAILED;
  }

}
