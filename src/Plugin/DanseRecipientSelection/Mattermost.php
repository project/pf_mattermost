<?php

namespace Drupal\pf_mattermost\Plugin\DanseRecipientSelection;

use Drupal\push_framework\Plugin\DanseRecipientSelection\DirectPush;

/**
 * Plugin implementation of DANSE.
 *
 * @DanseRecipientSelection(
 *   id = "mattermost",
 *   label = @Translation("Mattermost"),
 *   description = @Translation("Marks notifications to be sent to mattermost only.")
 * )
 */
class Mattermost extends DirectPush {

  /**
   * {@inheritdoc}
   */
  public function directPushChannelId(): string {
    return 'mattermost';
  }

}
