<?php

namespace Drupal\pf_mattermost\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings as FrameworkSettings;

/**
 * Configure Push Framework Mattermost settings.
 */
class Settings extends FrameworkSettings {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pf_mattermost_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pf_mattermost.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['mattermost_details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['mattermost_details']['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $this->pluginConfig->get('domain'),
      '#description' => $this->t('Provide the domain of your Mattermost instance, e.g. mattermost.example.com'),
    ];
    $form['mattermost_details']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $this->pluginConfig->get('token'),
      '#description' => $this->t('Read <a href="https://docs.mattermost.com/developer/personal-access-tokens.html">here</a> on how to generate your token.'),
    ];
    $form['mattermost_details']['channel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel ID'),
      '#default_value' => $this->pluginConfig->get('channel_id'),
      '#description' => $this->t('You find the channel ID in your Mattermost client when you open the "About Channel" popup from the top of the respective channel.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginConfig
      ->set('domain', $form_state->getValue('domain'))
      ->set('token', $form_state->getValue('token'))
      ->set('channel_id', $form_state->getValue('channel_id'));
    parent::submitForm($form, $form_state);
  }

}
